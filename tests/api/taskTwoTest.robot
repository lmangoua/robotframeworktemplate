# Author: lmangoua
# Date: 15/06/21

*** Settings ***
Documentation  Simple Login test case
Library  SeleniumLibrary

*** Variables ***
${browser}  chrome
${url}  https://opensource-demo.orangehrmlive.com/

*** Test Cases ***
LoginTest With Valid Credentials
    [Tags]  Smoke
    [Setup]  open browser  ${url}   ${browser}

    loginToApplication

    [Teardown]  close browser

    #add logs to console
    log to console  Test Executed Successfuly!!!

LoginTest With Invalid Credentials
    [Tags]  Regression
    [Setup]  open browser  ${url}   ${browser}

    maximize browser window

    NegativeLoginToApplication

    [Teardown]  close browser

    #add logs to console
    log to console  Test Executed Successfuly!!!

#execute command below to run this test
#robot tests/api/taskTwoTest.robot

*** Keywords ***
loginToApplication
    input text  id:txtUsername    admin
    input text  name:txtPassword     admin123
    click button  xpath://input[@value='LOGIN']
#    close browser

    #to return current url
    ${currentUrl}   get location
    log to console  Current URL: ${currentUrl}

    #to do validation (check that the current url has 'dashboard')
    should contain  ${currentUrl}   dashboard

NegativeLoginToApplication
    input text  id:txtUsername    admin
    input text  name:txtPassword     admin124
    click button  xpath://input[@value='LOGIN']
#    close browser

    #to return current url
    ${currentUrl}   get location
    log to console  Current URL: ${currentUrl}

    #to do validation (check that the current url has 'dashboard')
    should contain  ${currentUrl}   dashboard