# Author: lmangoua
# Date: 15/06/21

*** Settings ***
#Documentation   Simple example using SeleniumLibrary
Library  SeleniumLibrary
#Library    pabot.SharedLibrary    SeleniumLibrary
#Library  SeleniumLibrary
#Library  AppiumLibrary
#Library  Selenium2Library

*** Variables ***
${browser}  chrome
${url}  https://demo.nopcommerce.com/

*** Test Cases ***
LoginTest
#    create webdriver    chrome  executable_path = "resources/drivers/chromedriver"
#    create webdriver    Chrome  executable_path = "/usr/local/bin/chromedriver"
#    create webdriver    Chrome  executable_path = "/usr/local/bin/chromedriver"
    open browser  ${url}   ${browser}
#    open  browser   https://demo.nopcommerce.com/   chrome

    loginToApplication

    close browser

    #add logs to console
    log to console  Test Executed Successfuly!!!

#execute command below to run this test
#robot tests/api/taskOneTest.robot      OR
#robot tests/api/*.robot                TO RUN all the test classes under 'tests/api/'         OR
#pabot --processes 2 -d results -o Output.xml Tests *.robot                                    OR
#pabot --processes 2 --outputdir Report /Users/ab01367/Workspace/Python/robotframeworktemplate/tests/api/*.robot

*** Keywords ***
loginToApplication
    click link  xpath://a[contains(text(), 'Log in')]
    input text  id:Email    pavanoltraining@gmail.com
    input text  id:Password     Test@123
    click element   xpath://button[@class='button-1 login-button']
