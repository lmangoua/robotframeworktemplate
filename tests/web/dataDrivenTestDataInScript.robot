# Author: lmangoua
# Date: 15/07/21

*** Settings ***
Documentation  This test suite is using Test Data in the Script
Library  SeleniumLibrary
#Resource  resources/login_resources.robot
#Resource  ${EXECDIR}${/}resources/login_resources.robot
Resource  ../../resources/login_resources.robot
Variables  ../../data/yamlFiles/loginData.yaml
#Variables  ${EXECDIR}${/}data/loginData.yaml
Suite Setup  Open My Browser
Suite Teardown  Close My Browser
Test Template  Invalid Login

*** Variables ***

#*** Test Cases ***      username        password
#from tuto: https://www.youtube.com/watch?v=xoZ36eh8V2A&ab_channel=SDET-QAAutomationTechie
#minute: 23:35
*** Test Cases ***
#to read variables from a yaml file
#LoginTest With Invalid Credentials: Empty Password      ${validUsername}     ${emptyPassword}
LoginTest With Invalid Credentials: Empty Password      admin@yourstore.com     ${EMPTY}
LoginTest With Invalid Credentials: Wrong Password      admin@yourstore.com     test123
LoginTest With Invalid Credentials: Wrong Username, Right Password      ad@yourstore.com     admin
LoginTest With Invalid Credentials: Wrong Username, Empty Password      ad@yourstore.com     ${EMPTY}
LoginTest With Invalid Credentials: Wrong Username, Wrong Password      ad@yourstore.com     test123

#execute command below to run
# robot tests/web/dataDrivenTestDataInScript.robot

#to run test by passing *** variables ***
# robot -v LoginUrl:https://admin-demo.nopcommerce.com/  tests/web/dataDrivenTestDataInScript.robot

*** Keywords ***
Invalid Login
    [Arguments]  ${username}    ${password}
    Input Username  ${username}
    Input Password  ${password}
    Click Login Button
    Validate Error Message

    log to console  **** Invalid Login Test Executed Successfuly ***
