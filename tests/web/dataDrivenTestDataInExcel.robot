# Author: lmangoua
# Date: 18/07/21

*** Settings ***
Documentation    This test suite is using Test Data in Excel sheet
Library  SeleniumLibrary
Library  DataDriver  ../../data/excelFiles/loginData.xlsx   sheet_name=Login
#Library  ${EXECDIR}${/}data/excelFiles/loginData.xlsx   sheet_name=Login
#Another option by ignoring to sheetName
#Library  DataDriver  ../../data/excelFiles/loginData.xlsx
Resource  ../../resources/login_resources.robot
Suite Setup  Open My Browser
Suite Teardown  Close My Browser
Test Template  Invalid Login

*** Test Cases ***
#to read variables from a yaml file
LoginTestWithExcel By using ${username} and ${password}
#We can also do it by ignoring the params
#LoginTestWithExcel By Ignoring The Params using ${username} and ${password}
#ValidLoginTestWithExcel using ${username} and ${password}
#InvalidLoginTestWithExcel using ${username} and ${password}

#execute command below to run
# robot tests/web/dataDrivenTestDataInExcel.robot

*** Keywords ***
Invalid Login
    [Arguments]  ${username}    ${password}
    Input Username  ${username}
    Input Password  ${password}
    Click Login Button
    Validate Error Message

    log to console  **** Invalid Login Test Executed Successfuly ***

ValidLoginTestWithExcel
    [Arguments]  ${username}    ${password}
    Login  ${username}    ${password}
    Validate Error Message

    log to console  **** Invalid Login Test Executed Successfuly ***

InvalidLoginTestWithExcel
    [Arguments]  ${username}    ${password}
    Login  ${username}    ${password}
    Validate Error Message

    log to console  **** Invalid Login Test Executed Successfuly ***

Login
    [Arguments]  ${username}    ${password}
    Input Username  ${username}
    Input Password  ${password}
    Click Login Button

    log to console  **** Login ***

