#+--- slant 'Figlet: https://devhints.io/figlet'
#    ____        __          __
#   / __ )____  / /_  __  __/ /   ____ _   _____
#  / __  / __ \/ __ \/ / / / /   / __ \ | / / _ \
# / /_/ / /_/ / /_/ / /_/ / /___/ /_/ / |/ /  __/
#/_____/\____/_.___/\__, /_____/\____/|___/\___/

# Author: lmangoua
# Date: 20/07/21

*** Settings ***
Documentation    This test suite is using Test Data in CSV file
Library  SeleniumLibrary
Library  DataDriver  ../../data/csvFiles/loginData.csv
#Library  ${EXECDIR}${/}data/csvFiles/loginData.csv
Resource  ../../resources/login_resources.robot
Suite Setup  Open My Browser
Suite Teardown  Close My Browser
Test Template  Invalid Login

*** Test Cases ***
#to read variables from a yaml file
LoginTestWithExcel By using ${username} and ${password}
#We can also do it by ignoring the params
#LoginTestWithExcel By Ignoring The Params

#execute command below to run
# robot tests/web/dataDrivenTestDataInCsv.robot

*** Keywords ***
Invalid Login
    [Arguments]  ${username}    ${password}
    Input Username  ${username}
    Input Password  ${password}
    Click Login Button
    Validate Error Message

    log to console  **** Invalid Login Test Executed Successfuly ***