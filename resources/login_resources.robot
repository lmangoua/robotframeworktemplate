# Author: lmangoua
# Date: 15/07/21

*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary

*** Variables ***
${Browser}  chrome
${LoginUrl}  https://admin-demo.nopcommerce.com/

*** Keywords ***
Open My Browser
    open browser  ${LoginUrl}    ${Browser}
    maximize browser window
    log to console  **** Opened browser successfuly ****

Close My Browser
    close all browsers
    log to console  **** Closed browsers successfuly ****

Open Login Page
    go to  ${LoginUrl}
    log to console  **** Navigated to ${LoginUrl} successfuly ****

Input Username
    [Arguments]  ${username}
    input text  id:Email    ${username}

Input Password
    [Arguments]  ${password}
    input text  id:Password    ${password}

Click Login Button
    click button  xpath://button[contains(text(),'Log in')]
#    click element  xpath://button[contains(text(),'Log in')]

Click Logout Link
    click link  xpath://a[contains(text(),'Logout')]

Validate Error Message
    page should contain  Login was unsuccessful

Validate Dashboard Page
    page should contain  Dashboard