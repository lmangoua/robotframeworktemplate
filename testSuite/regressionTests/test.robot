# Author: lmangoua
# Date: 05/07/21

*** Settings ***
Documentation  Create a simple chrome browser test case with custom keywords

Library  SeleniumLibrary
Test Setup  open browser  ${url}   ${browser}
Test Teardown  close browser

*** Variables ***
${browser}  chrome
${url}  https://opensource-demo.orangehrmlive.com/

*** Test Cases ***
LoginTest With Valid Credentials
    [Tags]  Smoke
    LoginToApplication

    Logout from application

    #add logs to console
    log to console  **** Test Executed Successfuly ***

LoginTest With Invalid Credentials
    [Tags]  Regression

    maximize browser window

    Enter Username

    Enter Password

    Click Login Button

    Validate Dashboard

    #add logs to console
    log to console  **** Test Executed Successfuly ***

#execute command below to run this test
#robot testSuite/regressionTests/test.robot

*** Keywords ***
LoginToApplication
    input text  id:txtUsername    admin
    input text  name:txtPassword     admin123
    click button  xpath://input[@value='LOGIN']

    #to return current url
    ${currentUrl}   get location
    log to console  Current URL: ${currentUrl}

    #to do validation (check that the current url has 'dashboard')
    should contain  ${currentUrl}   dashboard

Enter Username
    input text  id:txtUsername    admin

Enter Password
    input text  name:txtPassword     admin124

Click Login Button
    click button  xpath://input[@value='LOGIN']

Validate Dashboard
    #to return current url
    ${currentUrl}   get location
    log to console  Current URL: ${currentUrl}

    #to do validation (check that the current url has 'dashboard')
    should contain  ${currentUrl}   dashboard

Logout from application
    click link  partial link:Welcome
    sleep  3s
    click link  link:Logout
    log to console  Logout Successfully!!!